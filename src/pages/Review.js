import React, { useEffect } from 'react'
import { useState } from 'react';
import { Button, ScrollView, StyleSheet, Text, TouchableNativeFeedback, TouchableOpacity, View } from 'react-native'
import ModalCustome from '../components/ModalCustome';
import ReviewCard from '../components/ReviewCard'
import Octicons from 'react-native-vector-icons/Octicons'

const Review = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [vote, setVote] = useState(5)

  return (
    <View style={styles.review}>
      <ModalCustome modalVisible={modalVisible} setModalVisible={setModalVisible} />
      <ScrollView style={styles.flex}>
        {/* <ReviewCard editable rating={3} />
                <ReviewCard  rating={4}/>
                <ReviewCard  rating={3}/>
                <ReviewCard  rating={1}/>
                <ReviewCard  rating={8}/>
                <ReviewCard  rating={10}/>
                <ReviewCard  rating={3}/>
                <ReviewCard  rating={4}/> */}
        <View style={styles.void}></View>
      </ScrollView>
      <View style={styles.plusBtn}>
        <TouchableOpacity
          onPress={() => {
            setModalVisible(true);
          }}
          scrollEnabled={false}
          style={{ zIndex: 9999, }}
        >
          <View style={styles.plusBtnTouch}>
            <Octicons name='plus' size={30} color='white' />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default Review

const styles = StyleSheet.create({
  review: {
    backgroundColor: '#000000',
    flex: 1,
    paddingTop: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  plusBtn: {
    width: 45,
    height: 45,
    borderRadius: 25,
    position: 'absolute',
    bottom: 4,
    right: 4,
  },
  plusBtnTouch: {
    width: 45,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#F7A7079C',
    justifyContent: 'center',
    alignItems: 'center',
  },
  flex: {
    paddingHorizontal: 19,
    flex: 1,
    zIndex: -1,
  },
  void: {
    height: 42,
  }
})
