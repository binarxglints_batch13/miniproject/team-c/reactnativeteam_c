import React , { useState } from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';

import Logo from '../components/Logo/';
import Form from '../components/Form/';
import { useDispatch } from 'react-redux';

const Register = (props) => {

  const dispatch = useDispatch()
  
  const submited = (value) => {
    dispatch({ type: 'SIGN_UP', data: value })
  }

  return (
    <View style={styles.container}>
      <Logo />
      <Form type='SIGN UP' submited={e => submited(e)} />
      <View style={styles.signupTextContent}>
        <Text style={styles.signupText}>Already have an account?</Text>
        <Text style={styles.signupButton} onPress={() => props.navigation.navigate('Login')}> Sign In</Text>
      </View>

    </View>
  )
}

export default Register

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: "#000000",
    alignItems: "center",
    justifyContent: "center"
  },
  signupTextContent: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginVertical: 16,
    color: '#ffffff',
    flexDirection: 'row'
  },
  signupText: {
    color: '#f5f5f5',
    fontSize: 15,
    fontWeight: 'normal'
  },
  signupButton: {
    color: '#fdc700',
    fontSize: 15,
    fontWeight: 'bold'
  }
});