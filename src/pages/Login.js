import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { useDispatch } from 'react-redux'
import Logo from '../components/Logo/';
import Form from '../components/Form/';

const Login = (props) => {
  const dispatch = useDispatch();

  const submited = (value) => {
    dispatch({ type: 'LOGIN', data: value })
  }

  return (
    <View style={styles.container}>
      <Logo />
      <Form type='Sign In' submited={e => submited(e)} />
      <View style={styles.signupTextContent}>
        <Text style={styles.signupText}>Don't have an account?</Text>
        <Text style={styles.signupButton} onPress={() => props.navigation.navigate('Register')}> Sign Up</Text>
      </View>

    </View>
  )
}

export default Login


const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: "#000000",
    alignItems: "center",
    justifyContent: "center"
  },
  signupTextContent: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginVertical: 16,
    color: '#ffffff',
    flexDirection: 'row'
  },
  signupText: {
    color: '#f5f5f5',
    fontSize: 15,
    fontWeight: 'normal'
  },
  signupButton: {
    color: '#fdc700',
    fontSize: 15,
    fontWeight: 'bold'
  }
});