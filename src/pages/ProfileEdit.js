import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Form from '../components/Form'
import HeaderProfile from '../components/HeaderProfile'
import ProfilePic from '../components/ProfilePic'
import { useDispatch } from 'react-redux'

const ProfileEdit = (props) => {
  const dispatch = useDispatch()
  const [photo, setPhoto] = useState({});

  const submited = (data) => {

    console.log(data);
    let formData = new FormData();
    formData.append('email', data.email)
    formData.append('fullname', data.fullname)
    formData.append('password', data.password)
    formData.append('username', data.username)
    cekImage() && formData.append('image', photo)
    dispatch({ type: 'UPDATE_USER', data: formData})
  }

  const tampil = () => {
    let coba = withImage;
    console.log('with Image',coba , 'photo', photo);
  }

  const cekImage = () => {
    return !photo.uri ? false : true;
  }

  useEffect(() => {
    console.log(photo);
    
    // setPhoto(temp)
    // console.log(photo);
  }, [photo])

  const menuClicked = () => {
    props.navigation.navigate('Profile')
  }

  return (
    <View style={styles.profileContainer}>
      <HeaderProfile type='edit' editClicked={() => editClicked()} menuClicked={() => menuClicked()}/>
      <View style={{top: 35, marginBottom: 35}}>
        <ProfilePic editable action={(e) => setPhoto(e)} />
      </View>
      <Form type='SAVE' submited={(e) => submited(e)} />
    </View>
  )
}

export default ProfileEdit

const styles = StyleSheet.create({
  profileContainer: {
    flex: 1, 
    backgroundColor: '#000000',
    alignItems: 'center',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
})
