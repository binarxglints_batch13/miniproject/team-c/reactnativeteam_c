import React, { useState } from 'react';
import {View, Text, TextInput, StyleSheet, Button, Image} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MareialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDispatch } from 'react-redux';
import Form from '../components/Form';
import HeaderProfile from '../components/HeaderProfile';
import ProfilePic from '../components/ProfilePic';


const Profile = (props) => {
  const dispatch = useDispatch()
  const [photo, setPhoto] = useState('')

  const submited = () => {
    dispatch({ type: 'LOGOUT'})
  }

  const menuClicked = () => {

  }

  

  const editClicked = () => {
    props.navigation.navigate('ProfileEdit');
  }

  return (
    <View style={styles.profileContainer}>
      <HeaderProfile type='read' editClicked={() => editClicked()} menuClicked={() => menuClicked()}/>
      <View style={styles.pictureContainer}>
        <ProfilePic />
      </View>
      <Form type='LOGOUT' submited={(e) => submited()} />
    </View>
  )
};

export default Profile;

const styles = StyleSheet.create({
  profileContainer: {
    flex: 1, 
    backgroundColor: '#000000',
    alignItems: 'center',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  pictureContainer: {
    marginTop: 32,
    marginBottom: 20,
  }
})
