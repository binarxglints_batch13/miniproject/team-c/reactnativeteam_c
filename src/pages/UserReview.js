import React, { useEffect, useState } from 'react'
import { Button, ScrollView, StyleSheet, Text, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import ModalCustome from '../components/ModalCustome'
import ReviewCard from '../components/ReviewCard'


const UserReview = (props) => {
  const dispatch = useDispatch();
  const [modalVisible, setModalVisible] = useState(false);
  const reviews = useSelector(state => state.review.review);


  const closeModalHandler = () => {
    setModalVisible(false);
  }

  const deleteHandler = () => {
    setModalVisible(true);
  }

  const editHandler = () => {

  }

  useEffect(() => {
    dispatch({ type: 'GET_USER_REVIEW' });
  }, [])

  return (
    <View style={styles.review}>
      <Button title='coba' onPress={()=>dispatch({ type: 'GET_USER_REVIEW' })} />
      <ModalCustome modalVisible={modalVisible} setModalVisible={closeModalHandler} />
      <ScrollView style={styles.flex}>
        {reviews.map((review, i) => {
          return <ReviewCard
            myReview
            data={review}
            key={i}
            editHandler={editHandler}
            deleteHandler={deleteHandler}
          />
        })}
      </ScrollView>
    </View>
  )
}

export default UserReview

const styles = StyleSheet.create({
  review: {
    backgroundColor: '#000000',
    flex: 1,
    paddingTop: 20,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  flex: {
    paddingHorizontal: 19,
    flex: 1,
    zIndex: -1,
  },
})
