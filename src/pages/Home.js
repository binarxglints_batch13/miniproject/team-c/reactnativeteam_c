import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet } from 'react-native'

import Genrebutton from '../components/Genrebutton'
import Moviesmain from '../components/Moviesmain'
import Searchframe from '../components/Searchframe'
import Background from '../components/Background'
import ModalCustome from '../components/ModalCustome'

import { connect } from 'react-redux'
import { useSelector, useDispatch } from 'react-redux'
import Genreframe from '../components/Genreframe'

const Home = props => {
  const dispatch = useDispatch();
  // const modal_redux = useSelector(state => state.review.modalState);
  // const headline_redux = useSelector(state => state.genre.headline);
  // const movies_redux = useSelector(state => state.movie.movieData)
  // const isloading = useSelector(state => state.movie.loading)
  // const isLoadingMore = useSelector(state => state.movie.loadingMore)
  const [commentInput, setCommentInput] = useState('')
  const [rating, setRating] = useState(0)

  useEffect(() => {
    dispatch({type: 'GET_DATA'})
    dispatch({type: 'GET_USER'})
  }, [])
  
  const handleComment = () => {
    let newPost = {
      rating,
      comment: commentInput
    }
    dispatch({type: 'POST_COMMENT', dataPost: newPost})
    setCommentInput('')
    dispatch({type: 'CLOSE_MODAL'})
  }

  const openModal = (data) => {
    dispatch({type: 'OPEN_MODAL', movieId: data._id});
  };

  const closeModal = () => {
    setCommentInput('')
    dispatch({type: 'CLOSE_MODAL'});
  };

  const navigateDetails = data => {
    dispatch({type: 'GET_MOVIE_ID', movieId: data._id});
    props.navigation.navigate('MovieDetails', {movieId: data._id});
  };

  const getMoreData = () => {
    dispatch({type: 'GET_MORE_MOVIE'})
  }

  return (
    <View style={{backgroundColor: 'yellow'}}>
      <View style={styles.backgroundBase}>
        {/* <ModalCustome
          modalState={modal_redux}
          modalClose={closeModal}
          onSubmitModal={closeModal}
          commentInput={(text) => setCommentInput(text)}
          handleComment={() => handleComment()}
          ratingHandler={(input) => setRating(input)}
          onPressTrash={() => closeModal()}
          value={commentInput}
        /> */}
        <Searchframe />
        <Genreframe />
        <View style={{marginHorizontal: 10, padding: 10}}>
          {/* <Text style={styles.headerText}>Hot{`${(headline_redux === '') ? " " : " " + headline_redux + " " }`}Movies</Text> */}
        </View>
        {/* <ScrollView>
        {isloading 
        ? <ActivityIndicator size='medium' color='green' /> 
        : movies_redux.map((item, index) => {
          if(index + 1 > movies_redux.length -1) {
            if(isLoadingMore) {
              return (
                <ActivityIndicator size='medium' color='green' />
              )
            }
            else {
              return (
                <TouchableOpacity key={index} style={{
                  alignSelf:'center',
                  alignItems:'center', 
                  justifyContent:'center',
                  backgroundColor:'white',
                  width: '80%',
                  padding:10,
                  borderRadius:20
                  }}
                  onPress={()=> getMoreData()}
                  >
                  <Text style={{color:'white'}}>More</Text>
                </TouchableOpacity>
                )
            };
          } else {
            return (
              <View key={index}>
                <Movies
                title={item.title}
                genre={item.genres}
                releaseYear={item.release_year}
                overview={item.synopsis}
                rating={item.averageRating === null ? "-" : item.averageRating}
                posterPath={item.poster}
                modalShow={() => openModal(item)}
                onPress={() => navigateDetails(item)}
                />
              </View>
              )
            };
            })
          }
        </ScrollView> */}
        
        
      </View>
    </View>
  );
};


export default Home;

const styles = StyleSheet.create({
  backgroundBase: {
    backgroundColor: '#213ba7',
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    height: '100%',
    paddingVertical: 10
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#ffffff'
  },
})
