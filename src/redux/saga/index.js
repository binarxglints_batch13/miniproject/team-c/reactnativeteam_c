import { all } from 'redux-saga/effects';
import reviewSaga from './reviewSaga';
import authSaga from './authSaga'

export default function* rootSaga() {
  yield all([reviewSaga(),authSaga()])
}