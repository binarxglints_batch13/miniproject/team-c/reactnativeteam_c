import { baseUrl } from '../../common/api';
import axios from 'axios';
import { put, takeLatest } from 'redux-saga/effects';
import { getHeadersFormData, getHeaders, getIdUser } from '../../common/function'


function* login(action) {
  try {
    const resLogin = yield axios({
        method: 'POST',
        url: `${baseUrl}/auth/signin`,
        data: action.data
    })
    console.log(resLogin);

    let { token : _, ...user} = resLogin.data.data;
    yield put({ type: 'LOGIN_SUCCESS', data: user })
    yield put({ type: 'ADD_TOKEN', data: resLogin.data.data.token })

  } catch (err) {
    console.log(err);
  }
}


function* signup(action) {
  
  try {
    const resLogin = yield axios({
      method: 'POST',
      url: `${baseUrl}/auth/signup`,
      data: action.data
    })

    let { token : _, ...user} = resLogin.data;
    yield put({ type: 'LOGIN_SUCCESS', data: user })
    yield put({ type: 'ADD_TOKEN', data: resLogin.data.token })
    console.log(resLogin.data)

  } catch (err) {
    console.log(err);
  }
}

function* update(action) {
  console.log("masuk coook");
  let header = yield getHeadersFormData();
  let userId = yield getIdUser();
  console.log('header',header);
  console.log('user',userId);
  
  try {
    const resUpdate = yield axios({
        method: 'PUT',
        url: `${baseUrl}/auth/user/${userId}`,
        headers: header,
        data: action.data
    })

    console.log(resUpdate);

    // let { token : _, ...user} = resLogin.data.data;
    // yield put({ type: 'LOGIN_SUCCESS', data: user })
    // yield put({ type: 'ADD_TOKEN', data: resLogin.data.data.token })

  } catch (err) {
    console.log(err);
  }
}


export default function* authSaga() {
  yield takeLatest('LOGIN', login)
  yield takeLatest('SIGN_UP', signup)
  yield takeLatest('UPDATE_USER', update)
}