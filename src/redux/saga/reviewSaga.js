import { baseUrl } from '../../common/api';
import axios from 'axios';
import { put, takeLatest } from 'redux-saga/effects';
import { getHeaders, getIdUser } from '../../common/function';


function* getUserReview() {
  try {
    console.log('masuk');
    let header = yield getHeaders();
    let id = yield getIdUser();
    console.log(header);
    console.log(id);
    const resReview = yield axios({
      method: 'GET',
      url: `${baseUrl}/api/v1/reviews/user/${id}`,
      headers: header,
    })
    console.log(resReview.data.allData);
    yield put({ type: 'GET_USER_REVIEW_SUCCESS', data: resReview.data.allData })

  } catch (err) {
    console.log(err);
  }
}




export default function* reviewSaga() {
  yield takeLatest('GET_USER_REVIEW', getUserReview)
}