import { combineReducers } from "redux";
import reviewReducer from './reviewReducer'
import authReducer from "./authReducer";

export default combineReducers({
  review: reviewReducer,
  auth: authReducer
})