const initState = {
  isLoading: false,
  review: [],
}

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case 'GET_USER_REVIEW':
      return {
        ...state,
        isLoading: true
      }
    case 'GET_USER_REVIEW_SUCCESS':
      return {
        ...state,
        isLoading: false,
        review: action.data
      }
    default:
      return state
  }
}

export default authReducer;