const initState = {
  isLoading: false,
  isLogin: false,
  user: {},
  token: '',
  
}

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        isLoading: true
      }
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        isLoading: false,
        user: action.data
      }
    case 'ADD_TOKEN':
      return {
        ...state,
        token: action.data,
        isLogin: true
      }
    case 'DELETE_TOKEN':
      return {
        ...state,
        token: '',
        isLogin: false
      }
    case 'LOGOUT':
      return {
        ...state,
        isLogin: false,
        user: {},
        token: ''
      }
    case 'SIGN_UP':
      return {
        ...state,
        isLoading: true
      }
    case 'UPDATE_USER':
      return {
        ...state,
        isLoading: true
      }
    default:
      return state
  }
}

export default authReducer;