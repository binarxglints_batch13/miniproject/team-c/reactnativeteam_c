import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Image, Touchable, TouchableOpacity } from 'react-native'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { useSelector } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons'
import BottomModal from './BottomModal';
import { baseUrl } from '../common/api';

const ProfilePic = (props) => {
  const image = useSelector(state => state.auth.user.image);
  const [photo, setPhoto] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [local, setLocal] = useState(true);

  const openCamera = () => {
    launchCamera({}, response => {
      if (response.didCancel) {
        console.log(response);
      } else if (response.errorCode) {
        console.log(response);
      } else if (response.errorMessage) {
        console.log(response);
      } else {
        setPhoto(response.assets[0].uri)
        let temp = {
          name: response.assets[0].fileName,
          type: response.assets[0].type,
          uri: response.assets[0].uri
        }
        props.action(temp)
        setLocal(true)
      }

    });
  }
  const openFile = () => {
    launchImageLibrary({maxHeight: 2000, maxWidth: 2000}, response => {
      if (response.didCancel) {
        console.log(response);
      } else if (response.errorCode) {
        console.log(response);
      } else if (response.errorMessage) {
        console.log(response);
      } else {
        setPhoto(response.assets[0].uri)
        let temp = {
          name: response.assets[0].fileName,
          type: response.assets[0].type,
          uri: response.assets[0].uri
        }
        props.action(temp)
        setLocal(true)
      }
    });
  }

  const detelePicture = () => {
    setPhoto('');
    props.action({});
    setLocal(true)
  }

  const action = (option) => {
    setModalVisible(false)
    switch (option) {
      case 'camera':
        openCamera();
        break;
      case 'folder':
        openFile();
        break;
      case 'trash':
        detelePicture();
        break;
      default:
        break;
    }
  }

  useEffect(() => {
    image && setPhoto(image);
    image && setLocal(false);
  }, [])

  return (
    <View >
      <View style={styles.imageContainer}>
        { photo == '' ?
          <Ionicons name='person-circle-outline' size={128} color='gray' style={styles.defaultPic} />
          :
          local ? 
            <Image source={{uri: photo}} style={styles.image} />
            :
            <Image source={{uri: baseUrl+photo}} style={styles.image} />
        }
      </View>
      { props.editable &&
        <View style={styles.editContainer}>
          <TouchableOpacity onPress={()=>setModalVisible(true) }>
            <MaterialCommunity name="pencil-outline" size={24} color="black"/> 
          </TouchableOpacity>
        </View>
      }
      <BottomModal modalVisible={modalVisible} action={(e)=>action(e)}/>
    </View>
  )
}

export default ProfilePic

const styles = StyleSheet.create({
  imageContainer: {
    width: 120,
    height: 120,
    backgroundColor: 'lightgray',
    borderRadius: 60,
  },
  image: {
    width: 120,
    height: 120,
    borderRadius: 60,
  },
  defaultPic: {
    position: 'relative',
    top: -8,
    left: -4,
  },
  editContainer: {
    position: 'absolute',
    right: 4,
    bottom: 4,
    backgroundColor: 'white',
    padding: 4,
    borderRadius: 20,
  }
})
