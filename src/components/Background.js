import React from 'react'
import { StyleSheet, View, Text } from 'react-native'

const Background = () => {
  return (
    <View style={styles.backgroundBase}>

    </View>
  )
}

export default Background;

const styles = StyleSheet.create({
  backgroundBase: {
    backgroundColor: '#000000',
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    height: '100%',
    paddingVertical: 10
  },
})
