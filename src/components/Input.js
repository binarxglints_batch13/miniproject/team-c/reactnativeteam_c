import React, { useEffect, useState } from "react";
import { StyleSheet, TextInput, View, Text, TouchableOpacity } from "react-native";
import Ionicons from 'react-native-vector-icons/Ionicons'

const Input = (props) => {
  const [secure, setSecure] = useState(false);
  const [bottomLineColor, setBottomLineColor] = useState('#ffffff');
  const [validations, setValidations] = useState([]);
  const [focused, setFocused] = useState(false);

  useEffect(() => {
    !props.pattern && setValid(true);
    props.secure && setSecure(true);
    setBottomLineColor('#ffffff');
  }, [])
 
  const handleValidation = (value) => {
    const { pattern } = props;
    if (!pattern) return true;

    const conditions = pattern.map(rule => new RegExp(rule[0], 'g'));
    return conditions.map(condition => condition.test(value));
  }

  const cekValid = (value) => {
    setValidations(value);

    if(value.includes(false)){
      setBottomLineColor('#EC521E');
      return false;
    } else {
      setBottomLineColor('#ffffff');
      return true;
    }
  }

  const onChange = (value) => {
    const isValid = handleValidation(value);
    let check = cekValid(isValid);

    props.onValidation(check);
    props.onChangeText(value);
  }

  return (
    <View>
      <TextInput
        underlineColorAndroid={bottomLineColor}
        placeholder={props.placeholder}
        secureTextEntry={secure}
        value={props.value}
        placeholderTextColor='#919191'
        style={[styles.inputBox, props.secure && styles.secure]}
        onChangeText={value => onChange(value)}
        onFocus={()=>{setFocused(true)}}
        onBlur={()=>{setFocused(false)}}
        editable={!props.readOnly}
      />
      { focused &&
        <View style={styles.warningContainer}>
          {validations.map((val, i) => !val && <Text style={styles.warning} key={i}>{props.pattern[i][1]}</Text>)}
        </View>
      }
      { props.secure && 
        <View style={styles.eye}>
          <TouchableOpacity onPress={()=> setSecure(!secure)}>
            { secure ?
              <Ionicons name='eye-off-outline' size={20} color='gray'/>
              :
              <Ionicons name='eye-outline' size={20} color='gray'/>
            }
          </TouchableOpacity>
        </View>
      }
    </View>
  );
}

export default Input;

const styles = StyleSheet.create({
  inputBox: {
    width: 300,
    borderRadius: 8,
    paddingHorizontal: 15,
    fontSize: 16,
    color: '#f5f5f5',
    marginVertical: 10,
  },
  warning: {
    color: '#EC521E',
    fontSize: 12,
    textAlign: 'right'
  },
  warningContainer: {
    paddingHorizontal: 10,
    position: "absolute",
    top: 55,
    paddingHorizontal: 10,
    backgroundColor: '#000000cf',
    zIndex: 1,
    right: 0,
  },
  secure: {
    paddingRight: 35,
  },
  eye: {
    position: 'absolute',
    right: 10,
    top: 23,
  }
});