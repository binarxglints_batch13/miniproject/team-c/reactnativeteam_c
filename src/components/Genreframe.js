import React, { useState } from 'react'
import { View, TouchableOpacity, Text, TouchableHighlight, StyleSheet } from 'react-native'
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'

const Genreframe = (props) => {
  return (
    <TouchableOpacity style={{ ...styles.box, backgroundColor: props.colorBG }} onPress={() => { props.onClick() }}>
      <MaterialCommunityIcon name='movie' />
      <Text>{props.name}</Text>
    </TouchableOpacity>
  )
};

export default Genreframe;

const styles = StyleSheet.create({
  box: {
    flex: 1,
    alignItems: 'center',
    height: 45,
    justifyContent: 'space-evenly',
    borderRadius: 10,
    backgroundColor: 'white',
    marginTop: 20,
    marginHorizontal: 2,
    flexDirection: 'row'
  }
});

