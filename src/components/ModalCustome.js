import React from 'react'
import { StyleSheet, Text, View, Modal, TouchableOpacity, TextInput } from 'react-native'
import Ratting from './Ratting';

const ModalCustome = ({ modalVisible, setModalVisible }) => {
  return (
    <View style={{ zIndex: 0, }}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.textHeading}>
              How do you think about this movie?
            </Text>
            <View>
              <Ratting multiple number={0} />
            </View>
            <TextInput
              style={styles.input}
              placeholder='Write a headline for your review here'
              placeholderTextColor='lightgray'
            />
            <TextInput
              style={[styles.input, styles.inputComment]}
              placeholder='Write your review here'
              placeholderTextColor='lightgray'
              multiline={true}
              numberOfLines={5}
              textAlignVertical='top'
            />
            <View style={styles.btnContainer}>
              <TouchableOpacity
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}
              >
                <Text style={styles.btnCancel}>
                  Review later
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}
              >
                <Text style={styles.btnSubmit}>
                  Submit
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  )
}

export default ModalCustome

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: '#0000009C',
  },
  modalView: {
    width: '90%',
    backgroundColor: "#FFE7AB",
    borderRadius: 20,
    padding: 21,
    alignItems: "center",
    shadowColor: "#000",
    elevation: 5
  },
  textHeading: {
    fontSize: 16,
    fontWeight: 'bold',
    lineHeight: 19,
  },
  inputComment: {
    height: 100
  },
  input: {
    marginBottom: 10,
    backgroundColor: 'white',
    paddingVertical: 6,
    paddingHorizontal: 11,
    width: '95%',
    borderRadius: 10,
    color: 'black',
    fontSize: 12,
  },
  btnSubmit: {
    backgroundColor: 'black',
    color: 'white',
    paddingHorizontal: 20,
    paddingVertical: 6,
    borderRadius: 10,
    fontWeight: 'bold',
    fontSize: 16,
  },
  btnContainer: {
    width: '95%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginVertical: 4,
  },
  btnCancel: {
    color: 'gray',
  }
})
