import React, { useState } from 'react'
import { View, Text, TouchableOpacity, Dimensions, TextInput, StyleSheet } from 'react-native'
import Entypo from 'react-native-vector-icons/Entypo'

const Searchframe = () => {
  const [searchInput, setSearchInput] = useState('')
  return (
    <View>
      <TextInput 
        placeholder='Find Movies...'
        placeholderTextColor='lightgray' 
        value={searchInput}  
        style={styles.searchBox} 
        onChangeText={(e)=>{
          setSearchInput(e)
        }}
      />
      <TouchableOpacity style={styles.button}>
        <Entypo name='magnifying-glass' size={22} color='gray' />
      </TouchableOpacity>
    </View>
  )
}

export default Searchframe;

const styles = StyleSheet.create({
  searchBox: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 20,
    paddingVertical: 6,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: 'white',
    color: 'black',
    paddingRight: 40,
    paddingLeft: 20, 
    fontSize: 16,
  },
  button: {
    position: 'absolute',
    right: 28,
    top: 10,
  }
})
