import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text
} from 'react-native';

const Logo = () => {
  return (
    <View style={styles.container}>
      <Image style={{ width: 120, height: 100 }}
        source={require('../images/movapplogo.png')} />
      <Text style={styles.logoText}>MovReact</Text>
    </View>
  )
}

export default Logo


const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  logoText: {
    marginVertical: 15,
    fontSize: 20,
    color: '#ffffff'
  }
});