import React, { useEffect, useState } from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Octicons from 'react-native-vector-icons/Octicons'
import Ratting from './Ratting';
import { baseUrl } from '../common/api'

const ReviewCard = (props) => {

  let myReview = false;
  let editable = false;

  props.myReview ? myReview = editable = true : null;
  props.editable ? editable = true : null;

  useEffect(() => {
  }, [props.data])

  return (
    <View style={styles.reviewCard}>
      {myReview ?
        <>
          <View style={styles.head}>
            <View style={styles.poster}>
              <Image
                style={styles.poster}
                source={{
                  uri: `${baseUrl}${props.data.movie.poster}`
                }}
              />
            </View>
            <View style={styles.rightHead}>
              <View>
                <View style={styles.rightHeadContent}>
                  <Text style={styles.headline}>{props.data.headline}</Text>
                </View>
                <View style={styles.rightHeadContent}>
                  <Text>Reviewed {props.data.movie.release_date}</Text>
                </View>
                <View style={styles.ratingContainer}>
                  <View>
                    <Ratting number={props.data.rating} />
                  </View>
                  <Text style={styles.rating}>
                    <Text style={styles.bold}>{props.data.rating}</Text>
                    /10
                  </Text>
                </View>
              </View>
              <View style={styles.rightHeadContent}>
                <TouchableOpacity
                  onPress={() => {
                    props.editHandler();
                  }}
                >
                  <View style={[styles.btn, styles.btnMyReview]}>
                    <Octicons name='pencil' size={13} color='white' />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    props.deleteHandler();
                  }}
                >
                  <View style={[styles.btn, styles.btnMyReview]}>
                    <Ionicons name='trash' size={13} color='white' />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <Text style={styles.headlineBotom}>{props.data.headline}</Text>
        </>
        :
        <View style={styles.head}>
          <View style={styles.img}>
          </View>
          <View style={styles.rightHeadContainer}>
            <View>
              <View style={styles.rightHeadContent}>
                <View style={styles.ratingContainer}>
                  <View>
                    {/* <Ratting number={props.rating} />  */}
                  </View>
                  <Text style={[styles.rating, styles.otherRating]}>
                    <Text style={styles.bold}>9</Text>
                    /10
                  </Text>
                </View>
                <Text style={styles.headline}>Great! (2020)</Text>
              </View>
              <View style={styles.rightHeadContent}>
                <Text>Reviewer: </Text>
                <Text style={styles.bold}>aigooo</Text>
              </View>
            </View>
            {editable &&
              <View style={styles.rightHead}>
                <TouchableOpacity>
                  <View style={[styles.btn]}>
                    <Octicons name='pencil' size={13} color='white' />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={[styles.btn, styles.btnOtherReview]}>
                    <Ionicons name='trash' size={13} color='white' />
                  </View>
                </TouchableOpacity>
              </View>
            }
          </View>
        </View>
      }
      <View>
        <Text style={styles.comment}>{props.data.comment}</Text>
      </View>
    </View>
  )
}

export default ReviewCard

const styles = StyleSheet.create({
  reviewCard: {
    backgroundColor: '#FFFFFF',
    marginBottom: 14,
    borderRadius: 20,
    paddingHorizontal: 13,
    paddingTop: 15,
    paddingBottom: 13,
  },
  img: {
    width: 40,
    height: 40,
    backgroundColor: 'gray',
    borderRadius: 20,
    marginRight: 8,
  },
  head: {
    flexDirection: 'row',
    marginBottom: 11,
  },
  rightHeadContent: {
    flexDirection: 'row',
    alignSelf: 'baseline'
  },
  headline: {
    fontWeight: 'bold',
    fontSize: 18,
    lineHeight: 22,
  },
  headlineBotom: {
    fontWeight: 'bold',
    fontSize: 18,
    lineHeight: 22,
    marginBottom: 11,
  },
  rating: {
    marginRight: 8,
    fontSize: 14,
    marginLeft: 11,
  },
  otherRating: {
    marginLeft: 1,
  },
  ratingContainer: {
    alignItems: 'center',
    flexDirection: 'row',

  },
  bold: {
    fontWeight: 'bold',
  },
  comment: {
    textAlign: 'justify',
  },
  poster: {
    width: 80,
    height: 120,
    backgroundColor: 'gray',
    marginRight: 16,
  },
  btn: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: '#F7A707',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnMyReview: {
    marginRight: 15,
    marginBottom: 4,
  },
  btnOtherReview: {
    position: 'relative',
    top: 4,
  },
  rightHead: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  rightHeadContainer: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
  }
})
