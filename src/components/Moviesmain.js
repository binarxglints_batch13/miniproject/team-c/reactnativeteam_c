import React from 'react'
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'

const Moviesmain = (props) => {
  return (
    <TouchableOpacity style={styles.container} onPress={() => props.navigateTo()}>
      <View style={styles.movieContainer}>
        <View style={styles.movieBox}>
          <View style={styles.contentContainer}>
            <MaterialCommunityIcon name="movie-open" size={35} />
          </View>
          <View style={{ marginVertical: 15, marginHorizontal: 25 }}>
            <Text style={{ textAlign: 'justify' }}> Test Lorem Lorem Ipsum  </Text>
          </View>
          <View style={styles.footerMovie}>
            <View style={styles.footerMovieContent}>
              <TouchableOpacity>
                <MaterialCommunityIcon name='share-variant'
                  size={23} />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity>
                <MaterialCommunityIcon name='message-text-outline'
                  size={23} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  )
};

export default Moviesmain;

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('screen').width - 50,
    alignSelf: 'center',
  },
  movieBox: {
    alignItems: 'center',
    backgroundColor: 'white',
    height: '100%',
    width: Dimensions.get('screen').width - 50,
    paddingVertical: '10%',
    borderRadius: 20,
    justifyContent: 'space-between'
  },
  movieContainer: {
    height: '70%',
    paddingVertical: 10
  },
  contentContainer: {
    borderWidth: 1,
    borderColor: 'black',
    width: Dimensions.get('screen').width - 100,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center'
  },
  footerMovie: {
    flexDirection: 'row',
    borderTopWidth: 1,
    width: Dimensions.get('screen').width - 100,
    justifyContent: 'space-between',
    paddingVertical: 10
  },
  footerMovieContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 50,
  }
})

