import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import MareialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import { useSelector } from 'react-redux';

const HeaderProfile = (props) => {

  const username = useSelector(state => state.auth.user.username)

  return (
    <View style={styles.topBar}>
      <View style={styles.topLeft}>
        <TouchableOpacity onPress={() => props.menuClicked()}>
          <View style={styles.topLeft}>
            {props.type === 'edit' &&  <MareialCommunity name="close" size={24} color="black" />}
            {props.type === 'read' &&  
              <>
                <Text style={styles.topTitle}>{username}</Text>
                <MareialCommunity name="menu-down" size={30} color="black" style={styles.menu}/>
              </>
            }
          </View>
        </TouchableOpacity>
        {props.type === 'edit' &&  <Text style={styles.topTitle}>Edit Profile</Text>}
      </View>
      { props.type === 'read' && 
        <TouchableOpacity onPress={() => props.editClicked()}>
          <View>
            <MareialCommunity name="pencil-outline" size={24} color="black" />
          </View>
        </TouchableOpacity>
      }
    </View>
  )
}

export default HeaderProfile

const styles = StyleSheet.create({
  topBar: {
    width: '100%',
    backgroundColor: 'white',
    alignItems: 'center',
    height: 62,
    flexDirection: 'row',
    paddingHorizontal: 15,
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: 'black',
  },
  topLeft: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  topTitle: {
    fontSize: 18, 
    color: 'black', 
    marginLeft: 10,
    fontWeight: 'bold',
  },
  menu: {
    position: 'relative',
    top: 3,
  }
})
