import React, { useEffect, useRef, useState } from 'react'
import { StyleSheet, Text, View, Animated, TouchableOpacity } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';



const Ratting = (props) => {
  const widthAnimation = useRef(new Animated.Value(0)).current;
  const [multiSars, setMultiSars] = useState([])
  const [ratingCount, setRatingCount] = useState(props.number)

  useEffect(() => {
    Animated.timing(widthAnimation, {
      toValue: (props.number * 2),
      duration: 1000,
      useNativeDriver: true,
    }).start();

    if (props.multiple) {
      resetStars(props.number);
    }

  }, [props.number])

  const resetStars = (starCount) => {
    let arr = [];
    for (let i = 0; i < 10; i++) {
      if (i < starCount) {
        arr[i] = true
      } else {
        arr[i] = false
      }
    }
    setMultiSars(arr);
  }

  const starClicked = (index) => {
    resetStars(index + 1);
    setRatingCount(index + 1);
  }


  const cobaAnimation = {
    width: (props.number * 2),
  }

  return (
    <View>
      {props.multiple ?
        <View>
          <View style={styles.starsContainer}>
            {multiSars.map((star, i) => {
              return star ?
                <TouchableOpacity onPress={() => { starClicked(i) }} key={i}>
                  <Ionicons name='star' size={20} color='#FFC200' />
                </TouchableOpacity>
                :
                <TouchableOpacity onPress={() => { starClicked(i) }} key={i}>
                  <Ionicons name='star' size={20} color='#979797' />
                </TouchableOpacity>
            })}
          </View>
          <Text style={styles.ratingCounter}> Your rating: {ratingCount}</Text>
        </View>
        :
        <View>
          <View>
            <Ionicons name='star' size={20} color='#979797' />
          </View>
          <Animated.View style={[styles.singleStarTop, cobaAnimation]}>
            <Ionicons name='star' size={20} color='#FFC200' />
          </Animated.View>
        </View>
      }
    </View>
  )
}

export default Ratting

const styles = StyleSheet.create({
  singleStarTop: {
    overflow: 'hidden',
    width: 0,
    height: 20,
    position: 'absolute',
  },
  starsContainer: {
    flexDirection: 'row',
    width: '100%',
    marginTop: 7,
    justifyContent: 'space-between',
  },
  ratingCounter: {
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    marginVertical: 14,
  }
})
