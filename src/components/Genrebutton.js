import React, { useState } from 'react';
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Genreframe from './Genreframe';
import { useDispatch, useSelector, connect } from 'react-redux';

const Genrebutton = (props) => {
  const dispatch = useDispatch()
    const [buttonState, setButtonState] = useState('')
    const genre = useSelector(state => state.genre)
    const actionHandler = () => {
        dispatch({type: 'ACTION'})
        setButtonState('buttonOne')
        dispatch({type: 'GET_MOVIE_BY_GENRE', genre :'Action'})
    }
    const romanceHandler = () => {
        dispatch({type: 'ROMANCE'})
        setButtonState('buttonTwo')
        dispatch({type: 'GET_MOVIE_BY_GENRE', genre :'Romance'})
    }
    const comedyHandler = () => {
        dispatch({type: 'THRILLER'})
        setButtonState('buttonThree')
        dispatch({type: 'GET_MOVIE_BY_GENRE', genre :'Thriller'})
    }
    const animeHandler = () => {
        dispatch({type: 'COMEDY'})
        setButtonState('buttonFour')
        dispatch({type: 'GET_MOVIE_BY_GENRE', genre :'Comedy'})
    }
    const resetHandler = () => {
        dispatch({type: 'RESET'})
        setButtonState('')
        dispatch({type: 'GET_DATA'})
    }

  return (
    <View style={styles.container}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <Text style={{ ...styles.text, fontSize: 20 }}>Best Genre</Text>
        <TouchableOpacity>
          <Text style={styles.text}>More {'>>'}</Text>
        </TouchableOpacity>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <Genreframe name="Action" colorBG={buttonState === 'buttonOne' ? 'orange' : 'white'}
          onClick={() => {
            props.actionClicked()
            setButtonState('buttonOne')
          }} />
        <Genreframe name="Romance" colorBG={buttonState === 'buttonTwo' ? 'orange' : 'white'}
          onClick={() => {
            props.romanceClicked()
            setButtonState('buttonTwo')
          }} />
        <Genreframe name="Thiller" colorBG={buttonState === 'buttonThree' ? 'orange' : 'white'}
          onClick={() => {
            props.thrillerClicked()
            setButtonState('buttonThree')
          }} />
        <Genreframe name="Comedy" colorBG={buttonState === 'buttonFour' ? 'orange' : 'white'}
          onClick={() => {
            props.comedyClicked()
            setButtonState('buttonFour')
          }} />
      </View>
    </View>
  )
};

export default Genrebutton;

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('screen').width - 50,
    marginVertical: 10,
    height: 100,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  text: {
    fontWeight: 'bold',
    color: 'white',
  }
});
