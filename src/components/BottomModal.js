import React from 'react'
import { Alert, Modal, Pressable, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';

const BottomModal = (props) => {

  return (
    <View>
      <Modal
        animationType="slide"
        transparent={true}
        fullScreen={false}
        visible={props.modalVisible}
        onRequestClose={() => {
          props.action('cancel');
        }}
      >
        <TouchableWithoutFeedback onPress={()=>{
          props.action(!props.modalVisible)
        }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View style={styles.optionContainer}>
                <TouchableOpacity style={styles.buttonContainer} onPress={()=> props.action('camera')}>
                  <Ionicons name='camera-outline' size={28} color='gray' style={styles.button} />
                </TouchableOpacity>
                <Text>Camera</Text>
              </View>
              <View style={styles.optionContainer}>
                <TouchableOpacity style={styles.buttonContainer} onPress={()=> props.action('folder')} >
                  <Ionicons name='folder-outline' size={24} color='gray' style={styles.button} />
                </TouchableOpacity>
                <Text>Folder</Text>
              </View>
              <View style={styles.optionContainer}>
                <TouchableOpacity style={styles.buttonContainer} onPress={()=> props.action('trash')} >
                  <Ionicons name='trash-outline' size={24} color='gray' style={styles.button} />
                </TouchableOpacity>
                <Text>Delete</Text>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </View>
  )
}

export default BottomModal

const styles = StyleSheet.create({
  centeredView: {
    justifyContent: "flex-end",
    marginTop: 'auto',
    height: '100%'
  },
  modalView: {
    backgroundColor: "white",
    padding: 35,
    alignItems: "center",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  button: {
    borderRadius: 100,
  },
  buttonContainer: {
    height: 45,
    width: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 25,
    borderColor: 'gray',
  },
  optionContainer: {
    marginHorizontal: 20,
    alignItems: 'center'
  },  
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
})
