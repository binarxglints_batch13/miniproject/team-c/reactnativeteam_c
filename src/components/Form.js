import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';
import { useSelector } from 'react-redux';
import Input from './Input';

const Form = (props) => {
  const [fullname, setFullname] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [valid, setValid] = useState([false,false,false,false]);
  const [readOnly, setReadOnly] = useState(false);
  const user = useSelector(state => state.auth.user)
  
  useEffect(() => {
    props.type === 'Sign In' && setValid([true,true,false,false]);
    props.type === 'SAVE' && setValid([true, true, true, false])
    if (props.type === 'LOGOUT' || props.type === 'SAVE'){
      setFullname(user.fullname);
      setUsername(user.username);
      setEmail(user.email);
    }
    props.type === 'LOGOUT' && setReadOnly(true);
  }, [])

  const isValid = () => {
    return !valid.includes(false);
  }

  const updateValid = (index, value) => {
    let current = valid;
    current[index] = value;
    setValid(current);
  }

  const buttonClicked = () => {
    if (props.type === 'LOGOUT') {
      props.submited()
    }else {
      let result = isValid();
      console.log(result);
      if (result) {
        if (props.type === 'Sign In') {
            props.submited({
              email, password
            })
        } else {
          props.submited({
            fullname, username, email, password
          })
        }
      }
    }
  }

  return (
    <View style={styles.container}>
      {(props.type == 'SIGN UP' || props.type == 'SAVE' || props.type == 'LOGOUT') && 
        <>
          <Input
            placeholder='Full Name'
            value={fullname}
            onChangeText={e => setFullname(e)}
            pattern={[
              ['(?!^$)','not empty'],
              ["^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$",'unvalid name']
            ]}
            onValidation={(e)=> updateValid(0, e)}
            readOnly={readOnly}
          />
          <Input 
            placeholder='Username'
            value={username}
            onChangeText={e => setUsername(e)}
            pattern={[
              ['(?!^$)','not empty'],
              ['^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]','unvalid username']
            ]}
            onValidation={(e)=> updateValid(1, e)}
            readOnly={readOnly}
          />
        </>
      }

      <Input 
        placeholder='Email'
        value={email}
        onChangeText={e => setEmail(e)} 
        pattern={[
          ['(?!^$)','not empty'],
          ['(?=(^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,}$))','unvalid email']
        ]}
        onValidation={(e)=> updateValid(2, e)}
        readOnly={readOnly}
      />

      {(props.type == 'SIGN UP' || props.type == 'SAVE') ?
        <Input
          placeholder='Password'
          secure
          value={password}
          onChangeText={e => setPassword(e)}
          pattern={[
            ['(?!^$)','not empty'],
            ['^.{8,}$','min 8 chars'],
            ['(?=.*\\d)', 'number required'], 
            ['(?=.*[A-Z])', 'uppercase letter required'], 
            ['(?=.*[@$!%*?&])', 'special character required']
          ]}
          onValidation={(e)=> updateValid(3, e)} 
        />
      :
        (props.type !== 'LOGOUT') &&
        <Input 
          placeholder='Password'
          secure
          value={password}
          onChangeText={e => setPassword(e)}
          pattern={[
            ['(?!^$)','not empty']
          ]}
          onValidation={(e)=> updateValid(3, e)}
        />
      }

      <TouchableOpacity onPress={() => buttonClicked()} style={styles.buttonContainer}>
        <Text style={styles.buttonText}>{props.type}</Text>
      </TouchableOpacity>

    </View>
  )
}

export default Form

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputBox: {
    width: 300,
    borderRadius: 8,
    paddingHorizontal: 15,
    fontSize: 16,
    color: '#f5f5f5',
    marginVertical: 10,
  }, 
  buttonText: {
    backgroundColor: '#ffffff',
    borderRadius: 30,
    marginVertical: 10,
    paddingVertical: 10,
    paddingHorizontal: 23,
    fontSize: 17,
    fontWeight: 'bold',
    color: '#000000',
    textAlign: 'center'
  },
  buttonContainer: {
    marginTop: 15,
  }
});