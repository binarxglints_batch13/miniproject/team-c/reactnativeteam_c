import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import UserReview from '../pages/UserReview';
import Homepage from '../pages/Homepage';
// import Profile from '../pages/Profile'
import ProfileStack from './ProfileStack';
import DasboardStack from './DasboardStack';
import { useSelector } from 'react-redux';
import { Image, StyleSheet } from 'react-native';
import { baseUrl } from '../common/api';

const Tab = createBottomTabNavigator();

const AppBottomTab = () => {
  const userImage = useSelector(state => state.auth.user.image)

  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        keyboardHidesTabBar: true,
      }}
      initialRouteName='Home'
    >
      <Tab.Screen name="Review" component={UserReview}
        options={{
          tabBarIcon: ({ focused, color }) => {
            let icon, size;
            focused ? icon = 'chatbubble' : icon = 'chatbubble-outline';
            focused ? color = 'black' : null;
            focused ? size = 30 : size = 26;
            return <Ionicons name={icon} size={size} color={color} style={{ transform: [{ scaleX: -1 }] }} />
          }
        }}
      />
      <Tab.Screen name="Home" component={DasboardStack}
        options={{
          tabBarIcon: ({ focused, color }) => {
            let icon, size;
            focused ? icon = 'home' : icon = 'home-outline';
            focused ? color = 'black' : null;
            focused ? size = 30 : size = 26;
            return <Ionicons name={icon} size={size} color={color} style={{ transform: [{ scaleX: -1 }] }} />
          }
        }}
      />
      <Tab.Screen name="ProfileStack" component={ProfileStack}
        options={{
          tabBarIcon: ({ focused, color }) => {
            if(!userImage){
              let icon, size;
              focused ? icon = 'person-circle' : icon = 'person-circle-outline';
              focused ? color = 'black' : null;
              focused ? size = 34 : size = 30;

              return <Ionicons name={icon} size={size} color={color} style={{ transform: [{ scaleX: -1 }] }} />
            }else {
              return <Image source={{uri: baseUrl+userImage}} style={ focused ? styles.imageBottomFocus : styles.imageBottom} />
            }
          }
        }}
      />
    </Tab.Navigator>
  ) 
}

export default AppBottomTab

const styles = StyleSheet.create({
  imageBottom: {
    width: 26,
    height: 26,
    borderRadius: 30,
    backgroundColor: 'gray'
  },
  imageBottomFocus: {
    width: 30,
    height: 30,
    borderRadius: 30,
    backgroundColor: 'gray'
  }
})

