import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import React from 'react'
import Profile from '../pages/Profile';
import ProfileEdit from '../pages/ProfileEdit';

const Stack = createStackNavigator();

const ProfileStack = () => {
  return (
    <Stack.Navigator headerMode='none'
      screenOptions={{
        ...TransitionPresets.SlideFromRightIOS
      }}
    >
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="ProfileEdit" component={ProfileEdit} />
    </Stack.Navigator>
  );
}

export default ProfileStack
