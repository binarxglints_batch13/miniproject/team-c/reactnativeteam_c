import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Login from '../pages/Login';
import Register from '../pages/Register';
import Homepage from '../pages/Homepage';
import AppBottomTab from './AppBottomTab';
import { useSelector } from 'react-redux';

const Stack = createStackNavigator();

const Appstack = () => {
  
  const isLogin = useSelector(state => state.auth.isLogin)
    
  return (
    <Stack.Navigator headerMode='none'>
      { isLogin ?
        <Stack.Screen name="AppBottomTab" component={AppBottomTab} />
        :
        <>
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Homepage" component={Homepage} />
          
        </>
      }
    </Stack.Navigator>
  );
}

export default Appstack;