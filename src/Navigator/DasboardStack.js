import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import React from 'react'
import Profile from '../pages/Profile';
import ProfileEdit from '../pages/ProfileEdit';
import Home from '../pages/Home'

const Stack = createStackNavigator();

const DasboardStack = () => {
  return (
    <Stack.Navigator headerMode='none'
      screenOptions={{
        ...TransitionPresets.ModalSlideFromBottomIOS
      }}
    >
      <Stack.Screen name="Home" component={Home} />
      {/* <Stack.Screen name="MovieDetails" component={HomeDetail} /> */}
      <Stack.Screen name="ProfileEdit" component={ProfileEdit} />
    </Stack.Navigator>
  );
}

export default DasboardStack
