import AsyncStorage from '@react-native-async-storage/async-storage';


export async function getHeaders() {
  const token = await getToken();

  return {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + token,
  };
}

export async function getHeadersFormData() {
  const token = await getToken();

  return {
    'Content-Type' : 'multipart/form-data',
    Authorization: 'Bearer ' + token,
  };
}

export const saveToken = async (token) => {
  await AsyncStorage.setItem('token', token);
}

export const saveUser = async (user) => {
  let stringUser = JSON.stringify(user)
  await AsyncStorage.setItem('user', stringUser);
}

export const getUser = async () => {
  let storeJson = await AsyncStorage.getItem('persist:root');
  let store = await JSON.parse(storeJson);
  let auth = await JSON.parse(store.auth);
  return auth.user;
}

export const getIdUser = async () => {
  let storeJson = await AsyncStorage.getItem('persist:root');
  let store = await JSON.parse(storeJson);
  let auth = await JSON.parse(store.auth);
  let id = auth.user.id;
  return id;
}

export const getToken = async () => {
  let token = await AsyncStorage.getItem('token');
  return token;
}
