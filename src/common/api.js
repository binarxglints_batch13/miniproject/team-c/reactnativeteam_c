// buar api dari BE
export const baseUrl = 'https://movie-development-v1.herokuapp.com';
// export const baseUrl = 'https://macan-api.gabatch13.my.id';

// fake api
export const fakeBaseUrl = 'https://mocki.io/v1/';
export const fakeImageBaseUrl = 'https://images.unsplash.com/';