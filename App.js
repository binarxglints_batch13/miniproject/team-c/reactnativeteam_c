import 'react-native-gesture-handler';
import React from 'react';
import { StatusBar } from 'react-native';

// react navigation
import { NavigationContainer } from '@react-navigation/native';
import Appstack from './src/Navigator/Appstack';

// redux
import { Provider } from 'react-redux';
import { store, persistor, sagaMiddleware } from './src/redux/store'

// redux persist
import { PersistGate } from 'redux-persist/integration/react'


const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          <StatusBar
            backgroundColor='#000000'
            barStyle='light-content'
          />
          <Appstack />
        </NavigationContainer>
      </PersistGate>
    </Provider>
  )
}

export default App;
